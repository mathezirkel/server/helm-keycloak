// Exports the user's fullName as callName + lastName
// If callName does not exist, use firstName
var callName = "";
var attributes = user.getAttributes();
if (attributes["callName"]) {
    callName = attributes["callName"][0];
} else {
    callName = user.firstName;
}
var fullName = callName + " " + user.lastName;
exports = fullName;
