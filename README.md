# Keycloak Helm chart

![Version: 7.1.0](https://img.shields.io/badge/Version-7.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 26.1.0](https://img.shields.io/badge/AppVersion-26.1.0-informational?style=flat-square)

This repository contains a [Helm](https://helm.sh/) chart to deploy [Keycloak](https://www.keycloak.org/) on Kubernetes.
Keycloak is an Open Source Identity and Access Management solution for modern Applications and Services.

We use the official Quarkus based docker image of [Keycloak](https://quay.io/repository/keycloak/keycloak).

The chart requires an existing [PostgreSQL](https://www.postgresql.org/) database,
[Traefik v2](https://traefik.io/traefik/) with its associated CRDs,
and [Argo Workflows](https://argoproj.github.io/workflows/) along with its CRDs if backups are enabled.

## Maintainers

| Name | Email | Url |
| ---- | ------ | --- |
| Felix Stärk | <felix.staerk@posteo.de> |  |
| Sven Prüfer | <sven@musmehl.de> |  |

## Installation

1. Create namespace.

    ```shell
    kubectl create namespace keycloak
    ```

2. Create a secret for the keycloak database crendentials:

    ```shell
    kubectl create secret generic database-credentials \
    --from-literal=database=keycloak \
    --from-literal=user=keycloakuser \
    --from-literal=password=CHANGEME \
    -n keycloak
    ```

3. Create a database user and a database in the internal database using the [script](./database-setup/database-setup.sql). Don't forget to modify the password.

4. Create an [initial admin user](https://www.keycloak.org/server/configuration/#creat).

5. Deploy the helm chart

   ```shell
   helm upgrade --install -n keycloak keycloak .
   ```

## Configuration

For more information about configuring Keycloak see the [official documentation](https://www.keycloak.org/documentation).

### Master realm

1. Adjust realm settings properly
2. Configure E-Mail (see below)

### Production realm

1. Create a separate realm for use in production.
2. Select this realm.
3. Create a group `admin`.
4. Assign the role `realm-admin` to the group `admin`.
5. Create a user and assign him to the group `admin`.

## Set up backup

The backup depends on a existing [restic](https://restic.readthedocs.io) repository available via sftp.

1. Initialize a restic repository.

    ```shell
    restic -r sftp:<ssh-alias>:<path-to-repository> init
    ```

2. Create a secret for the repository crendentials.

    ```shell
    kubectl create secret generic backup-restic-credentials \
    --from-literal=password=CHANGEME \
    -n keycloak
    ```

3. Create a secret for the SSH Key.

    ```shell
    ssh-keyscan -t rsa,ecdsa,ed25519 -H -p CHANGEME CHANGEME > /tmp/known_hosts
    ```

    ```shell
    kubectl create secret generic backup-ssh-credentials \
    --from-literal=hostname=CHANGEME \
    --from-literal=port=CHANGEME \
    --from-literal=user=CHANGEME \
    --from-literal=path=CHANGEME \
    --from-file=knownHosts=/tmp/known_hosts \
    --from-file=privateKey=CHANGEME \
    -n keycloak
    ```

4. Create a secret for mail credentials.
    For comprehensive documentation, refer to [python-sendmail](https://gitlab.com/mathezirkel/server/python-sendmail).

    ```shell
    kubectl create secret generic backup-mail-credentials \
    --from-literal=host=CHANGEME \
    --from-literal=port=CHANGEME \
    --from-literal=encryptionMode=CHANGEME \
    --from-literal=username=CHANGEME \
    --from-literal=password=CHANGEME \
    --from-literal=from=CHANGEME \
    --from-literal=to=CHANGEME \
    -n keycloak
    ```

5. Test your backup regularly! Detailed instructions can be found [here](https://gitlab.com/mathezirkel/server/helm-keycloak/-/tree/main/backup/).

## Values

### Backup

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| backup.cleanUpPolicy.keepDaily | int | `7` | Number of recent daily snapshots to keep |
| backup.cleanUpPolicy.keepLast | int | `3` | Number of recent snapshots to keep |
| backup.cleanUpPolicy.keepMonthly | int | `6` | Number of recent monthly snapshots to keep |
| backup.cleanUpPolicy.keepWeekly | int | `4` | Number of recent weekly snapshots to keep |
| backup.databaseDumpTempVolumeSize | string | `"1Gi"` | Size of the temporarily generated volume saving the database dumps |
| backup.enabled | bool | `true` | Enable the backup. |
| backup.failedJobsHistoryLimit | int | `2` | Number of failed jobs kept in history |
| backup.lastSuccessfulBackupThreshold | int | `24` | Maximum allowable age in hours for the most recent successful backup |
| backup.notification.existingSecret.encryptionModeKey | string | `"encryptionMode"` | Key containing the encryption method (Possible values: `ssl` or `starttls`) |
| backup.notification.existingSecret.fromKey | string | `"from"` | Key containing the email address sending the notification |
| backup.notification.existingSecret.hostKey | string | `"host"` | Key containing the hostname of the SMTP server |
| backup.notification.existingSecret.name | string | `"backup-mail-credentials"` | Name of an existing secret containing mail credentials for notification |
| backup.notification.existingSecret.passwordKey | string | `"password"` | Key containing the password for authentication |
| backup.notification.existingSecret.portKey | string | `"port"` | Key containing the SMTP port |
| backup.notification.existingSecret.toKey | string | `"to"` | Key containing an email address to receive backup notification |
| backup.notification.existingSecret.usernameKey | string | `"username"` | Key containing username for authentication |
| backup.pullPolicy | string | `"IfNotPresent"` | Workflow image pull policy |
| backup.restic.credentials.repository | object | `{"key":"password","name":"backup-restic-credentials"}` | Existing secret containing the repository credentials |
| backup.restic.credentials.repository.key | string | `"password"` | Key containing the password |
| backup.restic.credentials.repository.name | string | `"backup-restic-credentials"` | Name of the secret |
| backup.restic.credentials.ssh | object | `{"hostnameKey":"hostname","knownHostsKey":"knownHosts","name":"backup-ssh-credentials","pathKey":"path","portKey":"port","privateKeyKey":"privateKey","userKey":"user"}` | Existing secret containing the SSH credentials of the backup server |
| backup.restic.credentials.ssh.hostnameKey | string | `"hostname"` | Key containing the hostname |
| backup.restic.credentials.ssh.knownHostsKey | string | `"knownHosts"` | Key containing the known_hosts file |
| backup.restic.credentials.ssh.name | string | `"backup-ssh-credentials"` | Name of the secret |
| backup.restic.credentials.ssh.pathKey | string | `"path"` | Key containing the path |
| backup.restic.credentials.ssh.portKey | string | `"port"` | Key containing the port |
| backup.restic.credentials.ssh.privateKeyKey | string | `"privateKey"` | Key containing the private SSH key |
| backup.restic.credentials.ssh.userKey | string | `"user"` | Key containing the user |
| backup.restic.image.repository | string | `"restic/restic"` | Restic image repository |
| backup.restic.image.tag | string | `"0.17.0"` | Restic image tag Same as version on backup server |
| backup.schedule.backup | string | `"15 2 * * *"` | Schedule of the backup |
| backup.schedule.check | string | `"15 10 * * *"` | Schedule of the backup check |
| backup.schedule.clean | string | `"15 18 * * 0"` | Schedule of the backup clean up |
| backup.successfulJobsHistoryLimit | int | `1` | Number of successful jobs kept in history |

### Config

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| config.features | list | `["scripts"]` | List of additional keycloak features to enable |
| config.hostname.hostname | string | `"https://auth.mathezirkel-augsburg.de"` | Address at which is the server exposed. |
| config.hostname.hostnameBackchannelDynamic | bool | `true` | Enables dynamic resolving of backchannel URLs, including hostname, scheme, port and context path. |
| config.hostname.hostnameStrict | bool | `true` | Disables dynamically resolving the hostname from request headers. |
| config.http.httpEnabled | bool | `true` | Enables the HTTP listener. |
| config.logging.format | string | `"default"` | Logging format Possible values: default, json |
| config.logging.loglevel | string | `"WARN"` | Logging level Possible values: OFF, FATAL, ERROR, WARN, INFO, DEBUG, TRACE, ALL |
| config.logging.output | string | `"console"` | Comma-separated list of logging output streams. Possible Values: console, file, gelf |
| config.metrics.enabled | bool | `true` | Enable the metrics endpoint |
| config.proxy.proxyHeaders | string | `"xforwarded"` | The proxy headers that should be accepted by the server. Misconfiguration might leave the server exposed to security vulnerabilities. |
| config.proxy.trustedAddresses | list | `["127.0.0.1","10.42.0.0/24"]` | A list of trusted proxy addresses |

### Database

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| database.existingSecret.databaseKey | string | `"database"` | Key containing the database name |
| database.existingSecret.name | string | `"database-credentials"` | Name of an existing secret containing the database credentials |
| database.existingSecret.passwordKey | string | `"password"` | Key containing the user password |
| database.existingSecret.userKey | string | `"user"` | Key containing the database user |
| database.host | string | `"postgresql17.database.svc.cluster.local"` | External database service |
| database.port | int | `5432` | Port of the external database |
| database.vendor | string | `"postgres"` | Vendor of the external database |
| database.version | string | `"17.2-alpine"` | Database version |

### Deployment

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| fullnameOverride | string | `""` | String to fully override keycloak.fullname |
| image.pullPolicy | string | `"IfNotPresent"` | Keycloak image pull policy |
| image.repository | string | `"quay.io/keycloak/keycloak"` | Keycloak image repository |
| image.tag | string | Chart.appVersion | image.tag Keycloak image tag |
| livenessProbe.enabled | bool | `true` | Enable livenessProbe on Keycloak containers |
| livenessProbe.failureThreshold | int | `5` | Failure threshold for livenessProbe |
| livenessProbe.initialDelaySeconds | int | `30` | Initial delay seconds for livenessProbe |
| livenessProbe.periodSeconds | int | `5` | Period seconds for livenessProbe |
| livenessProbe.successThreshold | int | `1` | Success threshold for livenessProbe |
| livenessProbe.timeoutSeconds | int | `6` | Timeout seconds for livenessProbe |
| nameOverride | string | `""` | String to partially override keycloak.fullname |
| podManagementPolicy | string | `"OrderedReady"` | Pod management policy for the Keycloak statefulset |
| readinessProbe.enabled | bool | `true` | Enable readinessProbe on Keycloak containers |
| readinessProbe.failureThreshold | int | `5` | Failure threshold for readinessProbe |
| readinessProbe.initialDelaySeconds | int | `30` | Initial delay seconds for readinessProbe |
| readinessProbe.periodSeconds | int | `5` | Period seconds for readinessProbe |
| readinessProbe.successThreshold | int | `1` | Success threshold for readinessProbe |
| readinessProbe.timeoutSeconds | int | `2` | Timeout seconds for readinessProbe |
| replicaCount | int | `1` | Number of Keycloak replicas to deploy |
| resources.limits.memory | string | `"1024Mi"` | The memory limits for the Keycloak containers |
| resources.requests.cpu | string | `"125m"` | The requested cpu for the Keycloak containers |
| resources.requests.memory | string | `"256Mi"` | The requested memory for the Keycloak containers |
| service.port | int | `8080` | Keycloak service port |
| serviceAccount.create | bool | `true` | Enable creation of ServiceAccount for the Nexcloud application If set to false, the default ServiceAccount is used. |
| serviceAccount.name | string | keycloak.fullname | The name of the service account to use. |
| startupProbe.enabled | bool | `true` | Enable startupProbe on Keycloak containers |
| startupProbe.failureThreshold | int | `30` | Failure threshold for startupProbe |
| startupProbe.initialDelaySeconds | int | `180` | Initial delay seconds for startupProbe |
| startupProbe.periodSeconds | int | `20` | Period seconds for startupProbe |
| startupProbe.successThreshold | int | `1` | Success threshold for startupProbe |
| startupProbe.timeoutSeconds | int | `5` | Timeout seconds for startupProbe |
| terminationGracePeriodSeconds | int | `60` | Keycloak statefulset terminationGracePeriodSeconds |
| updateStrategy.rollingUpdate | object | `{}` | Keycloak statefulset rolling update configuration parameters |
| updateStrategy.type | string | `"RollingUpdate"` | Keycloak statefulset update strategy type |

### Ingress

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| ingress.additionalMiddlewares | list | `[{"name":"security-headers","namespace":"kube-public"}]` | List of additional middlewares to apply on the ingress |
| ingress.enabled | bool | `true` | Enable the ingress |
| ingress.entryPoint | string | `"websecure"` | List of traefik entrypoints to listen |
| ingress.host | string | `"auth.mathezirkel-augsburg.de"` | The hostname of the Keycloak instance |
| ingress.paths | list | `["/realms/","/resources/","/admin/"]` | List of paths which should be accessed from outside |
| ingress.tls.certResolver | string | `"lets-encrypt"` | Name of the certResolver configured via traefik |

## License

This project is licenced under [AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0), see [LICENSE](./LICENSE).

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.14.2](https://github.com/norwoodj/helm-docs/releases/v1.14.2)
