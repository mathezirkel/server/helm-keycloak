{{- if .Values.backup.enabled }}
apiVersion: argoproj.io/v1alpha1
kind: WorkflowTemplate
metadata:
  name: {{ printf "%s-backup-check" (include "keycloak.fullname" .) }}
  generateName: {{ printf "%s-backup-check-" (include "keycloak.fullname" .) }}
  namespace: {{ .Release.Namespace }}
  labels:
    app.kubernetes.io/name: {{ printf "%s-backup-check" (include "keycloak.fullname" .) }}
    app.kubernetes.io/component: backup
    {{- include "keycloak.labels" . | nindent 4 }}
spec:
  workflowMetadata:
    labels:
      app.kubernetes.io/name: {{ printf "%s-backup-check" (include "keycloak.fullname" .) }}
      app.kubernetes.io/component: backup
      {{- include "keycloak.labels" . | nindent 4 }}
  serviceAccountName: {{ printf "%s-backup" (include "keycloak.serviceAccountName" .) }}
  entrypoint: check
  ttlStrategy:
    secondsAfterSuccess: 600
  onExit: exit-handler
  volumes:
    - name: backup-ssh-credentials
      secret:
        secretName: {{ .Values.backup.restic.credentials.ssh.name }}
        defaultMode: 256
  templates:
    - name: check
      steps:
        - - name: check-integrity
            template: check-integrity
        - - name: check-existence
            template: check-existence
    - name: check-integrity
      script:
        image: "{{ .Values.backup.restic.image.repository }}:{{ .Values.backup.restic.image.tag }}"
        imagePullPolicy: {{ .Values.backup.pullPolicy }}
        command: [/bin/sh]
        source: |
          restic \
          -r \
          "sftp:${SSH_USER}@${SSH_HOSTNAME}:${REPOSITORY_PATH}" \
          -o sftp.args="-p ${SSH_PORT} -i /root/.ssh/id -o ServerAliveInterval=60 -o ServerAliveCountMax=240" \
          check
        env:
          - name: SSH_HOSTNAME
            valueFrom:
              secretKeyRef:
                name: {{ .Values.backup.restic.credentials.ssh.name }}
                key: {{ .Values.backup.restic.credentials.ssh.hostnameKey }}
          - name: SSH_PORT
            valueFrom:
              secretKeyRef:
                name: {{ .Values.backup.restic.credentials.ssh.name }}
                key: {{ .Values.backup.restic.credentials.ssh.portKey }}
          - name: SSH_USER
            valueFrom:
              secretKeyRef:
                name: {{ .Values.backup.restic.credentials.ssh.name }}
                key: {{ .Values.backup.restic.credentials.ssh.userKey }}
          - name: REPOSITORY_PATH
            valueFrom:
              secretKeyRef:
                name: {{ .Values.backup.restic.credentials.ssh.name }}
                key: {{ .Values.backup.restic.credentials.ssh.pathKey }}
          - name: RESTIC_PASSWORD
            valueFrom:
              secretKeyRef:
                name: {{ .Values.backup.restic.credentials.repository.name }}
                key: {{ .Values.backup.restic.credentials.repository.key }}
        volumeMounts:
          - name: backup-ssh-credentials
            mountPath: /root/.ssh/known_hosts
            subPath: {{ .Values.backup.restic.credentials.ssh.knownHostsKey }}
            readOnly: true
          - name: backup-ssh-credentials
            mountPath: /root/.ssh/id
            subPath: {{ .Values.backup.restic.credentials.ssh.privateKeyKey }}
            readOnly: true
    - name: check-existence
      script:
        image: "{{ .Values.backup.restic.image.repository }}:{{ .Values.backup.restic.image.tag }}"
        imagePullPolicy: {{ .Values.backup.pullPolicy }}
        command: [/bin/sh]
        source: |
          LATEST_SNAPSHOT_TIMESTAMP_HUMAN=$(restic \
              -r \
              "sftp:${SSH_USER}@${SSH_HOSTNAME}:${REPOSITORY_PATH}" \
              -o sftp.args="-p ${SSH_PORT} -i /root/.ssh/id -o ServerAliveInterval=60 -o ServerAliveCountMax=240" \
              snapshots latest |
              grep -E '^[0-9a-f]{8}' |
              awk '{print $2, $3}'
          )
          LATEST_SNAPSHOT_TIMESTAMP=$(date -d "${LATEST_SNAPSHOT_TIMESTAMP_HUMAN}" +%s)
          CURRENT_TIMESTAMP=$(date +%s)
          SNAPSHOT_AGE=$((CURRENT_TIMESTAMP - LATEST_SNAPSHOT_TIMESTAMP))
          THRESHOLD=$(({{ .Values.backup.lastSuccessfulBackupThreshold }} * 3600))
          echo "Latest snapshot: ${LATEST_SNAPSHOT_TIMESTAMP_HUMAN}"
          if [ ${SNAPSHOT_AGE} -lt ${THRESHOLD} ]; then
              echo "The latest backup is younger than {{ .Values.backup.lastSuccessfulBackupThreshold }} hours."
              exit 0
          else
              echo "The latest backup is older than {{ .Values.backup.lastSuccessfulBackupThreshold }} hours. Please have a look!"
              exit 1
          fi
        env:
          - name: SSH_HOSTNAME
            valueFrom:
              secretKeyRef:
                name: {{ .Values.backup.restic.credentials.ssh.name }}
                key: {{ .Values.backup.restic.credentials.ssh.hostnameKey }}
          - name: SSH_PORT
            valueFrom:
              secretKeyRef:
                name: {{ .Values.backup.restic.credentials.ssh.name }}
                key: {{ .Values.backup.restic.credentials.ssh.portKey }}
          - name: SSH_USER
            valueFrom:
              secretKeyRef:
                name: {{ .Values.backup.restic.credentials.ssh.name }}
                key: {{ .Values.backup.restic.credentials.ssh.userKey }}
          - name: REPOSITORY_PATH
            valueFrom:
              secretKeyRef:
                name: {{ .Values.backup.restic.credentials.ssh.name }}
                key: {{ .Values.backup.restic.credentials.ssh.pathKey }}
          - name: RESTIC_PASSWORD
            valueFrom:
              secretKeyRef:
                name: {{ .Values.backup.restic.credentials.repository.name }}
                key: {{ .Values.backup.restic.credentials.repository.key }}
        volumeMounts:
          - name: backup-ssh-credentials
            mountPath: /root/.ssh/known_hosts
            subPath: {{ .Values.backup.restic.credentials.ssh.knownHostsKey }}
            readOnly: true
          - name: backup-ssh-credentials
            mountPath: /root/.ssh/id
            subPath: {{ .Values.backup.restic.credentials.ssh.privateKeyKey }}
            readOnly: true
    - name: exit-handler
      steps:
        - - name: failure-notification
            arguments:
              parameters:
                - name: type
                  value: "Backup check"
            templateRef:
              name: {{ printf "%s-backup-exit" (include "keycloak.fullname" .) }}
              template: failure-notification
            when: "{{ `{{workflow.status}}` }} != Succeeded"
{{- end }}
