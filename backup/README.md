# Backups

## Manually create a backup

### Argo CLI

```shell
argo submit --from workflowtemplate/kecloak-backup -n keycloak
```

## Test a backup

1. Pull a snapshot from the restic repository.

    ```shell
    restic -r CHANGEME restore latest --target ./backup_io/
    ```

2. Start the dev setup

    ```shell
    docker compose up
    ```

    The dev setup automatically restores the `.sql` file from the backup. If you also want to test the `.dump` file, execute

    ```shell
    docker compose run --rm database pg_restore -U keycloakuser --clean -d databasename /backup_io/keycloak.dump
    ```

3. The Keycloak instance is available under [localhost:8080](localhost:8080).
Just login with your usual Keycloak account!
