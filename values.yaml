# Common Chart parameters

# -- String to partially override keycloak.fullname
# @section -- Deployment
nameOverride: ""
# -- String to fully override keycloak.fullname
# @section -- Deployment
fullnameOverride: ""


# Image

image:
  # -- Keycloak image repository
  # @section -- Deployment
  repository: quay.io/keycloak/keycloak
  # -- image.tag Keycloak image tag
  # @default -- Chart.appVersion
  # @section -- Deployment
  tag: ""
  # -- Keycloak image pull policy
  # @section -- Deployment
  pullPolicy: IfNotPresent

# -- Number of Keycloak replicas to deploy
# @section -- Deployment
replicaCount: 1
# -- Pod management policy for the Keycloak statefulset
# @section -- Deployment
podManagementPolicy: OrderedReady

updateStrategy:
  # -- Keycloak statefulset update strategy type
  # @section -- Deployment
  type: RollingUpdate
  # -- Keycloak statefulset rolling update configuration parameters
  # @section -- Deployment
  rollingUpdate: {}

# -- Keycloak statefulset terminationGracePeriodSeconds
# @section -- Deployment
terminationGracePeriodSeconds: 60

startupProbe:
  # -- Enable startupProbe on Keycloak containers
  # @section -- Deployment
  enabled: true
  # -- Initial delay seconds for startupProbe
  # @section -- Deployment
  initialDelaySeconds: 180
  # -- Period seconds for startupProbe
  # @section -- Deployment
  periodSeconds: 20
  # -- Timeout seconds for startupProbe
  # @section -- Deployment
  timeoutSeconds: 5
  # -- Failure threshold for startupProbe
  # @section -- Deployment
  failureThreshold: 30
  # -- Success threshold for startupProbe
  # @section -- Deployment
  successThreshold: 1

readinessProbe:
  # -- Enable readinessProbe on Keycloak containers
  # @section -- Deployment
  enabled: true
  # -- Initial delay seconds for readinessProbe
  # @section -- Deployment
  initialDelaySeconds: 30
  # -- Period seconds for readinessProbe
  # @section -- Deployment
  periodSeconds: 5
  # -- Timeout seconds for readinessProbe
  # @section -- Deployment
  timeoutSeconds: 2
  # -- Failure threshold for readinessProbe
  # @section -- Deployment
  failureThreshold: 5
  # -- Success threshold for readinessProbe
  # @section -- Deployment
  successThreshold: 1

livenessProbe:
  # -- Enable livenessProbe on Keycloak containers
  # @section -- Deployment
  enabled: true
  # -- Initial delay seconds for livenessProbe
  # @section -- Deployment
  initialDelaySeconds: 30
  # -- Period seconds for livenessProbe
  # @section -- Deployment
  periodSeconds: 5
  # -- Timeout seconds for livenessProbe
  # @section -- Deployment
  timeoutSeconds: 6
  # -- Failure threshold for livenessProbe
  # @section -- Deployment
  failureThreshold: 5
  # -- Success threshold for livenessProbe
  # @section -- Deployment
  successThreshold: 1

resources:
  requests:
    # -- The requested memory for the Keycloak containers
    # @section -- Deployment
    memory: 256Mi
    # -- The requested cpu for the Keycloak containers
    # @section -- Deployment
    cpu: 125m
  limits:
    # -- The memory limits for the Keycloak containers
    # @section -- Deployment
    memory: 1024Mi


# Config

config:
  hostname:
    # -- Address at which is the server exposed.
    # @section -- Config
    hostname: https://auth.mathezirkel-augsburg.de
    # -- Enables dynamic resolving of backchannel URLs, including hostname, scheme, port and context path.
    # @section -- Config
    hostnameBackchannelDynamic: true
    # -- Disables dynamically resolving the hostname from request headers.
    # @section -- Config
    hostnameStrict: true
  http:
    # -- Enables the HTTP listener.
    # @section -- Config
    httpEnabled: true
  proxy:
    # -- The proxy headers that should be accepted by the server.
    # Misconfiguration might leave the server exposed to security vulnerabilities.
    # @section -- Config
    proxyHeaders: xforwarded
    # -- A list of trusted proxy addresses
    # @section -- Config
    trustedAddresses:
      - "127.0.0.1"
      - "10.42.0.0/24"
  # -- List of additional keycloak features to enable
  # @section -- Config
  features:
    - scripts
  metrics:
    # -- Enable the metrics endpoint
    # @section -- Config
    enabled: true
  # See: https://www.keycloak.org/server/logging
  logging:
    # -- Comma-separated list of logging output streams.
    # @section -- Config
    # Possible Values: console, file, gelf
    output: console
    # -- Logging format
    # @section -- Config
    # Possible values: default, json
    format: default
    # -- Logging level
    # @section -- Config
    # Possible values: OFF, FATAL, ERROR, WARN, INFO, DEBUG, TRACE, ALL
    loglevel: WARN


# Database

database:
  # -- Vendor of the external database
  # @section -- Database
  vendor: postgres
  # -- Database version
  # @section -- Database
  version: 17.2-alpine
  # -- External database service
  # @section -- Database
  host: postgresql17.database.svc.cluster.local
  # -- Port of the external database
  # @section -- Database
  port: 5432
  existingSecret:
    # -- Name of an existing secret containing the database credentials
    # @section -- Database
    name: database-credentials
    # -- Key containing the database name
    # @section -- Database
    databaseKey: database
    # -- Key containing the database user
    # @section -- Database
    userKey: user
    # -- Key containing the user password
    # @section -- Database
    passwordKey: password


# Backup

backup:
  # -- Enable the backup.
  # @section -- Backup
  enabled: true
  schedule:
    # -- Schedule of the backup
    # @section -- Backup
    backup: "15 2 * * *"
    # -- Schedule of the backup check
    # @section -- Backup
    check: "15 10 * * *"
    # -- Schedule of the backup clean up
    # @section -- Backup
    clean: "15 18 * * 0"
  # -- Maximum allowable age in hours for the most recent successful backup
  # @section -- Backup
  lastSuccessfulBackupThreshold: 24
  cleanUpPolicy:
    # -- Number of recent snapshots to keep
    # @section -- Backup
    keepLast: 3
    # -- Number of recent daily snapshots to keep
    # @section -- Backup
    keepDaily: 7
    # -- Number of recent weekly snapshots to keep
    # @section -- Backup
    keepWeekly: 4
    # -- Number of recent monthly snapshots to keep
    # @section -- Backup
    keepMonthly: 6
  # -- Number of successful jobs kept in history
  # @section -- Backup
  successfulJobsHistoryLimit: 1
  # -- Number of failed jobs kept in history
  # @section -- Backup
  failedJobsHistoryLimit: 2
  # -- Workflow image pull policy
  # @section -- Backup
  pullPolicy: IfNotPresent
  # -- Size of the temporarily generated volume saving the database dumps
  # @section -- Backup
  databaseDumpTempVolumeSize: 1Gi
  restic:
    image:
      # -- Restic image repository
      # @section -- Backup
      repository: restic/restic
      # -- Restic image tag
      # Same as version on backup server
      # @section -- Backup
      tag: 0.17.0
    credentials:
      # -- Existing secret containing the SSH credentials of the backup server
      # @section -- Backup
      ssh:
        # -- Name of the secret
        # @section -- Backup
        name: backup-ssh-credentials
        # -- Key containing the hostname
        # @section -- Backup
        hostnameKey: hostname
        # -- Key containing the port
        # @section -- Backup
        portKey: port
        # -- Key containing the user
        # @section -- Backup
        userKey: user
        # -- Key containing the path
        # @section -- Backup
        pathKey: path
        # -- Key containing the known_hosts file
        # @section -- Backup
        knownHostsKey: knownHosts
        # -- Key containing the private SSH key
        # @section -- Backup
        privateKeyKey: privateKey
      # -- Existing secret containing the repository credentials
      # @section -- Backup
      repository:
        # -- Name of the secret
        # @section -- Backup
        name: backup-restic-credentials
        # -- Key containing the password
        # @section -- Backup
        key: password
  notification:
    existingSecret:
      # -- Name of an existing secret containing mail credentials for notification
      # @section -- Backup
      name: backup-mail-credentials
      # -- Key containing the hostname of the SMTP server
      # @section -- Backup
      hostKey: host
      # -- Key containing the SMTP port
      # @section -- Backup
      portKey: port
      # -- Key containing the encryption method (Possible values: `ssl` or `starttls`)
      # @section -- Backup
      encryptionModeKey: encryptionMode
      # -- Key containing username for authentication
      # @section -- Backup
      usernameKey: username
      # -- Key containing the password for authentication
      # @section -- Backup
      passwordKey: password
      # -- Key containing the email address sending the notification
      # @section -- Backup
      fromKey: from
      # -- Key containing an email address to receive backup notification
      # @section -- Backup
      toKey: to


# Service

service:
  # -- Keycloak service port
  # @section -- Deployment
  port: 8080


# Service Account

serviceAccount:
  # -- Enable creation of ServiceAccount for the Nexcloud application
  # @section -- Deployment
  # If set to false, the default ServiceAccount is used.
  create: true
  # -- The name of the service account to use.
  # @section -- Deployment
  # @default -- keycloak.fullname
  name: ""


# Ingress

ingress:
  # -- Enable the ingress
  # @section -- Ingress
  enabled: true
  # -- List of traefik entrypoints to listen
  # @section -- Ingress
  entryPoint: websecure
  # -- The hostname of the Keycloak instance
  # @section -- Ingress
  host: auth.mathezirkel-augsburg.de
  # -- List of additional middlewares to apply on the ingress
  # @section -- Ingress
  additionalMiddlewares:
    - name: security-headers
      namespace: kube-public
  # -- List of paths which should be accessed from outside
  # @section -- Ingress
  paths:
    - /realms/
    - /resources/
    - /admin/
  tls:
    # -- Name of the certResolver configured via traefik
    # @section -- Ingress
    certResolver: lets-encrypt
