-- Add user for postgresql
CREATE USER keycloakuser WITH PASSWORD 'CHANGEME';

-- Add database for nextcloud
CREATE DATABASE keycloak WITH OWNER=keycloakuser;